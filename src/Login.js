import React, { Component } from 'react';
import fire from 'firebase'
import 'firebase/auth';
import 'bootstrap/dist/css/bootstrap.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Input, Label } from 'reactstrap';

export default class Login extends Component {
  usuario = React.createRef();
  contraseña = React.createRef();

  constructor(props) {
    super(props);
    //Este enlace es necesario para hacer que `this` funcione en el callback
    this.login = this.login.bind(this);
    this.signup = this.signup.bind(this);
  }
  login(e) {
    e.preventDefault();
    var misusuario = this.usuario.current.value;
    var micontraseña = this.contraseña.current.value;

    fire
      .auth()
      .signInWithEmailAndPassword(misusuario, micontraseña)
      .then(u => {})
      .catch(function(error) {
        console.log(error);
      });
  }

  signup(e) {
    e.preventDefault();
    var miusuario = this.usuario.current.value;
    var micontraseña = this.contraseña.current.value;
    fire
      .auth()
      .createUserWithEmailAndPassword(miusuario, micontraseña)
      .then(u => {})
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    return (
            
      <div class="card-login" >
          <img src={'./assets/profile.jpg'}></img>
        <form >
          <div >
            <p htmlFor="exampleInputEmail">Correo electronico: </p>
            <input type="email" name="email" id="exampleInputEmail" ref={this.usuario}></input>
          </div>
          <div >
            <p htmlFor="exampleInputPassword">Contraseña: </p>
            <input type="password" name="password" id="exampleInputPassword" ref={this.contraseña} ></input>
          </div>
          <Button color="danger" type="submit"  onClick={this.login}>Login</Button>
          <Button style={{ display: 'inline', margin: 20}} color="danger" onClick={this.signup}>Registrarse</Button>
        </form>
      </div>
    );
  }
}
