import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Input, Label } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.css';
import fire from 'firebase'
import 'firebase/auth';

class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isToggleOn: true,
            contacts: [],
            search: [],
            productoBackup: [],
            textBuscar: '',
            detallesP: 0,
        };
    }

    componentDidMount() {
        fetch('https://rickandmortyapi.com/api/character')
            .then(res => res.json())
            .then((data) => {
                this.setState({
                    contacts: data.results,
                    productoBackup: data.results
                })
                console.log(this.state.contacts)
            })
            .catch(console.log)
    }

    filter(event) {
        console.log(event.target.value)
        //obtener datos de buscar
        var text = event.target.value
        //obtener datos de arrar
        const data = this.state.productoBackup
        const newData = data.filter(function (item) {
            //campo de Nombre
            const itemData = item.name.toUpperCase()
            console.log("itemdataaaaa", itemData)
            //variable de buscar
            const textData = text.toUpperCase()
            // filtrar si es verdadero o no
            return itemData.indexOf(textData) > -1
        })
        this.setState({
            contacts: newData,
            textBuscar: text,
        })
    }

    abrirModal = (id) => {
        console.log("recibiendo ID", this.state.contacts)
        this.state.contacts.filter(info => info.id === id).map(contact => console.log(contact))
        this.setState({
            abierto: !this.state.abierto,
            detallesP: id,
        });
    }


    filterIndividual(event) {
        console.log("evento filtro Individual", event.target.value)
        //obtener datos de buscar
        var text = event.target.value
        //obtener datos de arrar
        const data = this.state.productoBackup

        const elementoId = data.filter(function (item) {
            //campo de Nombre
            const itemData = item.id.toUpperCase()
            console.log("itemdataaaaa", itemData)
            //variable de buscar
            const textData = text.toUpperCase()
            // filtrar si es verdadero o no
            return itemData.indexOf(textData) > -1
        })
        this.setState({
            contacts: elementoId,
            textBuscar: text,
        })
    }
 logout(){
        //Cierro sesion
        fire.auth().signOut();
    }

    render() {
        const modalStyles = {
            position: "absolute",
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)'
        }
        return (
            <>
                <div>
                    <center>
                        <h1 style={{display: 'inline'}} >Lista de Contactos del ricanmorti</h1>
                        <Button style={{ display: 'inline', float: 'right', margin: 20}} color="danger" onClick={this.logout}>Cerrar sesion</Button>
                    </center>
                    <center>
                        <span>Busqueda</span>
                        <input class="form-control col-md-4" placeholder="Ingrese un nombre para buscar..." value={this.state.text} onChange={(text) => this.filter(text)} />
                    </center>

                    <div class="">
                        {this.state.contacts.map((contact) => (
                            <div class="card-body">

                                <div>
                                    <img class="profileContact" src={contact.image}></img>
                                </div>
                                <div >
                                    <h5 class="card-title">Name: {contact.name}</h5>
                                    <h6 class="card-subtitle mb-2 text-muted">Status: {contact.status}</h6>
                                    <h6 class="card-subtitle mb-2 text-muted">Specie: {contact.species}</h6>
                                    <h6 class="card-subtitle mb-2 text-muted">Localitation: {contact.location.name}</h6>
                                    <h6 class="card-subtitle mb-2 text-muted">Origin: {contact.origin.name}</h6>
                                    <Button color="success" onClick={(e) => this.abrirModal(contact.id)}>Mostrar Detalles</Button>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>

                <Modal isOpen={this.state.abierto} style={modalStyles}>
                    <ModalHeader>
                        Detalles del personaje
                    </ModalHeader>
                    <ModalBody>
                        <div class="">
                            {!this.state.contacts ? (<p>Cargando..</p>) : this.state.contacts.filter(info => info.id === this.state.detallesP).map(contact =>
                                <div class="" >

                                    <div>
                                        <img class="profileContact" src={contact.image}></img>
                                    </div>
                                    <div >
                                        <h5 class="card-title">{contact.name}</h5>
                                        <h6 class="card-subtitle mb-2 text-muted">{contact.species}</h6>
                                        <p class="card-text">{contact.origin.name}</p>
                                    </div>
                                </div>
                            )}
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" value="" onClick={this.abrirModal}>Cerrar</Button>
                    </ModalFooter>
                </Modal>
            </>


        )
    }


}

export default Form;