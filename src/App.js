import React, { Component } from 'react';
import fire from 'firebase'


import './App.css';
import Contacts from './components/contacts';
import 'firebase/auth';
import { useFirebaseApp, useUser } from 'reactfire';


import Login from './Login';

class App extends Component {
  
  state={
    user:{}
  }
  constructor(props){
    super(props);
  }
  componentDidMount(){
    this.authListener();
  }
  authListener(){
    
    //Compruebo si el usuario se ha loggeado
    fire.auth().onAuthStateChanged((user)=>{
      if(user){
        //Usuario loggeado
        this.setState({user});
      }else{
        //Usuario NO logueado
        this.setState({user:null});
      }
    });
  }
  render(){
    return (
      <div className="App">
        {/* Si el usuario esta loggeado voy a Home sino Login */}
        {this.state.user ? (<Contacts />) :(<Login/>)}
      </div>
    );
  }
}

export default App;

// function App() {
//   const firebase = useFirebaseApp();
  
//   const user = firebase.auth().currentUser;
//   return (
//     <div className="App">
// { user && <p>Usuario:{user.email}</p>}
// <Auth/>
//     </div>
// /* <Contacts></Contacts> */
//   );
// }

// export default App;


